import { createClient } from 'redis';

const { DB_PROTOCOL, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT } = process.env

const client = createClient({
    url: `${DB_PROTOCOL}://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}`
})

export const initializeDBConnection = async () => {
    try {
        await client.connect()
        console.log('Successfully connected to the CN DB (Redis)');
    } catch (error) {
        console.error(error);
    }
}

export const set = async(key: string, value: string) => {
    return await client.set(key, value)
}

export const get = async (key: string) => {
    return await client.get(key)
}

export const remove = async (key: string) => {
    return await client.del(key)
}
