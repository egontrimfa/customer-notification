import sendGridMail, { ClientResponse, MailDataRequired } from '@sendgrid/mail';

const SENDGRID_API_KEY: string = process.env.SENDGRID_API_KEY || 'SENDGRID_API_KEY'

sendGridMail.setApiKey(SENDGRID_API_KEY)

export const sendEmail = (mailOptions: MailDataRequired): Promise<[ClientResponse, {}]> => {
    return sendGridMail.send(mailOptions)
}