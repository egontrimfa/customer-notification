export interface EntryInterface {
    email: string,
    sku: string
}

export interface EmailInterface {
    email: string
}

export interface SKUInterface {
    sku: string
}

export interface NotifyInterface {
    sku: string,
    product_type_name: string
}