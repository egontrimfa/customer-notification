import { MailDataRequired } from '@sendgrid/mail';
import { Application, Router } from 'express';

import { get, set } from '../db'
import { EntryInterface, NotifyInterface, SKUInterface } from '../interfaces';
import { sendEmail } from '../utils/email';

const router: Router = new (Router as any)()

/**
 * @route POST /subscribe
 * @desc Subscribing an client's email address to a product SKU
 */
router.post('/subscribe', async (req, res, next) => {
    try {
        const { email, sku }: EntryInterface = req.body
        const entry = await get(sku)
        const emailsToSubscribe: String[] = []

        if (entry) {
            const alreadySubscribedEmails: String[] = JSON.parse(entry)
            emailsToSubscribe.push(...alreadySubscribedEmails)
        }

        if (emailsToSubscribe.includes(email)) {
            return res.status(409).send({ message: 'The provided email address is already subscribed to the product notification list!' })
        }

        emailsToSubscribe.push(email)
        await set(sku, JSON.stringify(emailsToSubscribe))

        return res.send({ emails: emailsToSubscribe })
    } catch (error) {
        next(error);
    }
})

router.post('/notify', async (req, res, next) => {
    try {
        const { sku, product_type_name: productTypeName }: NotifyInterface = req.body
        const entry = await get(sku)

        if(!entry) {
            return res.status(404).send({ message: 'There is no subscribed entry with the given SKU key.' })
        }

        const subscribedEmails: String[] = JSON.parse(entry)
        subscribedEmails.forEach(subscribedEmail => {
            const subject = `${productTypeName} is back on stock`
            const to = subscribedEmail
            const from = 'sapientia.egontrimfa@gmail.com'
            const html = `
                <p>Dear costumer,</p>
                <p>The product ${productTypeName}, with the SKU of ${sku} what you have searched for recetly is once again available. Go and grab it while you can.</p>
                <p>You received this email because you were subscribed to the product's notification list. If you did not request this, please ignore this email.</p>

                <p>Sincerely,</p>
                <p>Sapientia User</p>
            `

            sendEmail({ to, from, subject, html } as MailDataRequired).then(async () => {
                console.log(`Email sent to ${subscribedEmail} successfully!`);
                console.log('removed email: ', subscribedEmails.shift());
                await set(sku, JSON.stringify(subscribedEmails))
            })
        })

        return res.send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route DELETE /unsubscribe
 * @desc Unsubscribing a client's email address from a product SKU
 */
router.delete('/unsubscribe', async (req, res, next) => {
    try {
        const { email, sku }: EntryInterface = req.body
        const entry = await get(sku)

        if(!entry) {
            return res.status(404).send({ message: 'There is no subscribed entry with the given SKU key.' })
        }

        const subscribedEmails: String[] = JSON.parse(entry)
        const emailIndex = subscribedEmails.findIndex(subscribedEmail => subscribedEmail === email)

        if (emailIndex === -1) {
            return res.status(404).send({ message: 'The provided email was not subscribed to the given product.' })
        }

        subscribedEmails.splice(emailIndex, 1)
        await set(sku, JSON.stringify(subscribedEmails))
        
        return res.send({ emails: subscribedEmails })
    } catch (error) {
        next(error)
    }
})

export const mountRoutes = (app: Application) => {
    app.use('/', router)
}